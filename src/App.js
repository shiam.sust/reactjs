/* eslint-disable */
import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';
import Radium from 'radium';
//import Shiam from './Person/Shiam';

class App extends Component {
  state = {
    persons: [
      {id: 'dfgds', name: 'shiam chowdhury', age: 28},
      {id: 'gdsgh', name: 'Shajid', age: 27},
      {id: 'etbsf', name: 'Noyon', age: 23}
    ],
    showPersons: false
  }

  deletePersonHandler = (personIndex) => {
    const tempPersons = this.state.persons;
    tempPersons.splice(personIndex,1);
    this.setState({persons: tempPersons});
  }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    //alternate
    //const person = Object.assign({},this.state.persons[personIndex]);
    
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({
      persons: persons
    });  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        z
  }

  togglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow});
  }

  render(){
    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ':hover': {
        backgroundColor: 'lightgreen',
        color: 'black'
      }
    };

    let persons = null;
    if(this.state.showPersons){
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
            return <Person 
              click={() => this.deletePersonHandler(index)}
              name={person.name}
              agee={person.age}
              key={person.id}
              changed={(event) => this.nameChangedHandler(event, person.id)} />
          })}
        </div> 
      );
      style.backgroundColor = 'red';
      style[':hover'] = {
        backgroundColor: 'salmon',
        color: 'black'
      }
    }

    let classes = [];
    if(this.state.persons.length <= 2){
      classes.push('red');
    }if(this.state.persons.length <=1){
      classes.push('bold');
    }

    return (
      <div className="App">
        <h1>Welcome to react js world</h1>
        <p className={classes.join(' ')}>We are very excited</p>
        <button 
          style={style}
          onClick={this.togglePersonHandler}>Click Me</button>
        {persons}
      </div>
    );
  }
  
}

export default Radium(App);
